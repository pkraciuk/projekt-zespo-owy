﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.Comment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isi2.Projekt.Data.Repositories
{
    public class CommentRepository
    {
        private Entities entities = new Entities();
        
        public PagedListResultContract<CommentContract> GetComments(CommentFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.cmtx_comments.Include("usrx_users").AsQueryable();

            if (filter != null)
            {
                if (filter.EntryId.HasValue)
                    query = query.Where(c => c.entx_id == filter.EntryId);
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderBy(c => c.created).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

            var result = query.Select(c => new CommentContract
            {
                Id = c.cmtx_id,
                Text = c.text,
                Date = c.created,
                UserId = c.usrx_id,
                EntryId = c.entx_id,    
                UserName = c.usrx_users.login,
            }).ToList();

            return new PagedListResultContract<CommentContract> { Count = count, Items = result, Page = filter.Page };
        }

        public SaveContract AddComment(CommentContract comment)
        {
            SaveContract result = new SaveContract();
            entities.cmtx_comments.Add(new cmtx_comments
            {
                usrx_id = comment.UserId,
                created = comment.Date,
                text = comment.Text,
                entx_id = comment.EntryId,
            });

            entities.SaveChanges();
            result.Success = true;
            return result;
        }

    }
}
