﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.User;
using System.Web.Security;


namespace Isi2.Projekt.Data
{
    public class UserRepository
    {
        private Entities entities = new Entities();

        public SaveContract Register(UserContract user)
        {
            SaveContract result = new SaveContract();

            var existing = this.GetUsers(new UserFilterContract { Login = user.Login }, null);
            if (existing.Items.Count > 0)
            {
                result.Errors = "Ta nazwa użytkownika jest już zajęta!";
                return result;
            }

            existing = this.GetUsers(new UserFilterContract { Email = user.Email }, null);
            if (existing.Items.Count > 0)
            {
                result.Errors = "Podany adres email jest już zarejestrowany w bazie!";
                return result;
            }
            
            entities.usrx_users.Add(new usrx_users
            {
                login = user.Login,
                email = user.Email,
                city = user.City,
                banned = user.Banned,
                password = user.Password,
                password_reminder = user.Password_reminder,
                date_last_login = user.Date_last_login,
                date_created = user.Date_created,
                ban_reason = user.Ban_reason,
                is_mod = user.Is_mod,
                is_admin = user.Is_admin
            });

            entities.SaveChanges();

            result.Success = true;

            return result;
        }

        public SaveContract ChangePassword(string login, string oldPassword, string newPassword)
        {
            var query = from u in entities.usrx_users where u.login == login select u;
            foreach (usrx_users usr in query)
                usr.password = newPassword;
            
            entities.SaveChanges();

            return new SaveContract { Success = true };
        }

        public PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging)
        {
           
            var query = entities.usrx_users.AsQueryable();

            if (filter != null)
            {
                if (filter.Id.HasValue)
                    query = query.Where(c => c.usrx_id == filter.Id);
                if (!String.IsNullOrEmpty(filter.Login))
                    query = query.Where(c => c.login == filter.Login);
                if (!String.IsNullOrEmpty(filter.Password))
                    query = query.Where(c => c.password == filter.Password);
                if (!String.IsNullOrEmpty(filter.Email))
                    query = query.Where(c => c.email == filter.Email);
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderBy(c => c.usrx_id).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

            var result = query.Select(c => new UserContract
            {
                Id = c.usrx_id,
                Login = c.login,
                Is_admin = c.is_admin,
                Is_mod = c.is_mod,
                Ban_reason = c.ban_reason,
                Banned = c.banned,
                Date_created = c.date_created,
                City = c.city,
                Email = c.email,
                Date_last_login = c.date_last_login,
                Password = c.password,
                Password_reminder = c.password_reminder
            }).ToList();

            return new PagedListResultContract<UserContract> { Count = count, Items = result };
        }

        public bool BanUser(int userId, string banReason)
        {
            var user = entities.usrx_users.FirstOrDefault(x => x.usrx_id == userId);
            if (user == null)
            {
                return false;
            }
            if (banReason.Length > 200)
            {
                banReason = banReason.Substring(0,200);
            }
            user.banned = true;
            user.ban_reason = banReason;
            entities.SaveChanges();
            return true;
        }

        public bool UnBanUser(int userId)
        {
            var user = entities.usrx_users.FirstOrDefault(x => x.usrx_id == userId);
            if (user == null)
            {
                return false;
            }
            user.banned = false;
            user.ban_reason = string.Empty;
            entities.SaveChanges();
            return true;
        }

        public Dictionary<int, string> Users(string term) 
        {
            var users = entities.usrx_users.Where(x=>x.login.Contains(term)).Select(x => new { id = x.usrx_id, login = x.login }).ToDictionary(x => x.id,x=>x.login);
            //var users = entities.usrx_users.AsEnumerable().Select(x => new Dictionary<int, string> { { x.usrx_id, x.login } });
            return users;
        }

        public bool SetPermissions(int userId, bool isMod, bool IsAdmin) 
        {
            var user = entities.usrx_users.FirstOrDefault(x => x.usrx_id == userId);
            if (user == null)
            {
                return false;
            }
            user.is_mod = isMod;
            user.is_admin = IsAdmin;
            entities.SaveChanges();
            return true;
        }

        public int GetLoginUserId(string userLogin)
        {
            return entities.usrx_users.Single(x => x.login == userLogin).usrx_id;
        }

    }
}
