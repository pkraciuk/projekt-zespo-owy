﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts;
using System.Web.Security;
using Isi2.Projekt.Contracts.Entry;


namespace Isi2.Projekt.Data
{
    public class EntryRepository
    {
        private Entities entities = new Entities();

        public SaveContract AddEntry(EntryContract entry)
        {
            SaveContract result = new SaveContract();
            entities.entx_entry.Add(new entx_entry
            {
                created = DateTime.Now,
                usrx_id = entry.AuthorId,
                title = entry.Title,
                hot = false,
                deleted = false,
                path = entry.Path,
                sticky = false,
                content_type = string.Empty,
            });

            entities.SaveChanges();

            result.Success = true;

            return result;

        }

        public PagedListResultContract<EntryContract> GetEntries(EntryFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.entx_entry.AsQueryable();

            if (filter != null)
            {
                if (filter.Id.HasValue)
                {
                    query = query.Where(c => c.entx_id == filter.Id);
                }
                if (filter.Ids != null)
                {
                    query = query.Where(c => filter.Ids.Contains(c.entx_id));
                }
                if (filter.Published.HasValue)
                    query = query.Where(c => c.hot == filter.Published && c.deleted == filter.Deleted);
                if (filter.Deleted.HasValue)
                {
                    query = query.Where(c => c.deleted == filter.Deleted);
                }
                if (filter.UserId.HasValue)
                {
                    query = query.Where(x => x.usrx_id == filter.UserId);
                }
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderByDescending(c => c.sticky).ThenByDescending(c => c.created).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

           // var q = entities.votx_votes.AsQueryable();
            //var votes = q.Where(a => (a.entx_id == filter.Id));
            //int votesNumber =  votes.Count();
            //throw new DivideByZeroException();
            //string nowy = filter.Username;
            //throw new DivideByZeroException();
            var result = query.Select(c => new EntryContract
            {
                Id = c.entx_id,
                AuthorId = c.usrx_id,
                Created = c.created,
                Deleted = c.deleted,
                Published = c.hot,
                Title = c.title,
                File = c.path,
                Username = c.usrx_users.login,
                Sticky = c.sticky,
                VotesNumber = entities.votx_votes.AsQueryable().Where(a=>(a.entx_id == c.entx_id)).Count(),
                EntryAlreadyVotedFor = entities.votx_votes.AsQueryable().Where(d => (d.usrx_id == entities.usrx_users.AsQueryable().Where(b => (b.login == filter.Username)).FirstOrDefault().usrx_id && d.entx_id == c.entx_id)).Count() > 0 ? false : true,
                ImageAlreadyInFavourites = entities.fvtx_favourites.AsQueryable().Where(e => (e.usrx_id == entities.usrx_users.AsQueryable().Where(f => (f.login == filter.Username)).FirstOrDefault().usrx_id && e.entx_id == c.entx_id)).Count() > 0 ? true : false,
            }).ToList();
            return new PagedListResultContract<EntryContract> { Count = count, Items = result, Page = filter.Page };

        }

        public bool HideImage(int imageId)
        {
            var image = entities.entx_entry.FirstOrDefault(x => x.entx_id == imageId);
            if (image != null)
            {
                image.deleted = true;
                entities.SaveChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        public bool AddToMain(int imageId)
        {
            var image = entities.entx_entry.FirstOrDefault(x => x.entx_id == imageId);
            if (image != null)
            {
                image.hot = true;

                entities.SaveChanges();
            }
            else
            {
                return false;
            }
            return true;
        }
        
		public PagedListResultContract<EntryContract> GetFavouritesImagesForUser(EntryFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.fvtx_favourites.AsQueryable();
            var count = query.Count();
            query = query.Where(c => c.usrx_id == filter.Id);

            count = query.Count();


            query = query.OrderByDescending(c => c.entx_entry.created).Skip(paging.Page * paging.PageSize).Take(paging.Take);

            var result = query.Select(c => new EntryContract
            {
                //Id = c.entx_entry.usrx_id,
                Id = c.entx_id,
                AuthorId = c.usrx_id,
                Created = c.entx_entry.created,
                Deleted = c.entx_entry.deleted,
                Published = c.entx_entry.hot,
                Title = c.entx_entry.title,
                File = c.entx_entry.path,

                Username = entities.usrx_users.AsQueryable().Where(g => g.usrx_id == c.entx_entry.usrx_id).FirstOrDefault().login,

                VotesNumber = entities.votx_votes.AsQueryable().Where(a=>(a.entx_id == c.entx_id)).Count(),
                EntryAlreadyVotedFor = entities.votx_votes.AsQueryable().Where(d => (d.usrx_id == entities.usrx_users.AsQueryable().Where(b => (b.login == filter.Username)).FirstOrDefault().usrx_id && d.entx_id == c.entx_id)).Count() > 0 ? false : true,
                ImageAlreadyInFavourites = entities.fvtx_favourites.AsQueryable().Where(e => (e.usrx_id == entities.usrx_users.AsQueryable().Where(f => (f.login == filter.Username)).FirstOrDefault().usrx_id && e.entx_id == c.entx_id)).Count() > 0 ? true : false,
            }).ToList();

            return new PagedListResultContract<EntryContract> { Count = count, Items = result, Page = filter.Page };
        }

        public SaveContract AddImageToFavourites(EntryContract entry, int userId)
        {
            SaveContract result = new SaveContract();
            entities.fvtx_favourites.Add(new fvtx_favourites
                                             {
                                                 usrx_id = userId,
                                                 entx_id = entry.Id,
                                             });
            entities.SaveChanges();
            result.Success = true;
            return result;
        }
        public bool ImageAlreadyInFavourites(int entryId, string userName)
        {
            //var query = entities.fvtx_favourites.AsQueryable();
            //var query2 = entities.usrx_users.AsQueryable();

            //var user = query2.Where(a => (a.login == userName)).First();

            //query = query.Where(c => (c.usrx_id ==user.usrx_id  && c.entx_id == entryId) );
            //int count = query.Count();
            return entities.fvtx_favourites.AsQueryable().Where(e => (e.usrx_id == entities.usrx_users.AsQueryable().Where(f => (f.login == userName)).FirstOrDefault().usrx_id && e.entx_id == entryId)).Count() > 0 ? true : false;

        }
        public SaveContract VoteForEntry(EntryContract entry, int userId)
        {
            SaveContract result = new SaveContract();
            entities.votx_votes.Add(new votx_votes
            {
                usrx_id = userId,
                entx_id = entry.Id,
            });
            entities.SaveChanges();
            result.Success = true;
            return result;
        }
        public bool EntryAlreadyVotedFor(int entryId, string userName){

            //var query = entities.votx_votes.AsQueryable();

            //var query2 = entities.usrx_users.AsQueryable();

            //var user = query2.Where(a => (a.login == userName)).First();

            //query = query.Where(c => (c.usrx_id == user.usrx_id && c.entx_id == entryId));
            //int count = query.Count();


            return entities.votx_votes.AsQueryable().Where(d => (d.usrx_id == entities.usrx_users.AsQueryable().Where(b => (b.login == userName)).FirstOrDefault().usrx_id && d.entx_id == entryId)).Count() > 0 ? false : true;

        }
        public int VotesNumber(int entryId)
        {
            var query = entities.votx_votes.AsQueryable();
            var votes = query.Where(a => (a.entx_id == entryId));
            return votes.Count();
        }

        public int CommentVotesNumber(int userId, int commentId)
        {
            var query = entities.cvtx_votes_comment.AsQueryable();
            var votes = query.Where(a=>(a.usrx_id==userId && a.cmtx_id==commentId));
            return votes.Count();
        }

        public bool CommentAlreadyVotedFor(int userId, int commentId)
        {
            var query = entities.cvtx_votes_comment.AsQueryable();
            var votes = query.Where(a => (a.usrx_id == userId && a.cmtx_id == commentId));
            if (votes.Count() > 0)
            {
                return true;
            }
            return false;
        }

        public SaveContract AddVoteForComment(int userId, int commentId)
        {
            SaveContract result = new SaveContract();

            var query = entities.cmtx_comments.AsQueryable();
            var entryId = query.Where(a => (a.cmtx_id == commentId)).First().entx_id;
            entities.cvtx_votes_comment.Add(new cvtx_votes_comment
            {
                entx_id=entryId,
                usrx_id = userId,
                cmtx_id=commentId,
            });
            entities.SaveChanges();
            result.Success = true;
            throw new DivideByZeroException();
            return result;
        }
    }
}
