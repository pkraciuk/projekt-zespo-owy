﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.User;
using Isi2.Projekt.Contracts.Comment;
using Isi2.Projekt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Security;
using Isi2.Projekt.Contracts.Entry;
using Isi2.Projekt.Data.Repositories;

namespace Isi2.Projekt.Service
{
    public class Service : IService
    {
        private UserRepository userRepository = new UserRepository();
        private EntryRepository entryRepository = new EntryRepository();
        private CategoryRepository categoryRepository = new CategoryRepository();
        private CommentRepository commentRepository = new CommentRepository();

        public PagedListResultContract<CommentContract> GetComments(CommentFilterContract filter, PagedListRequestContract paging)
        {
            return commentRepository.GetComments(filter, paging);
        }

        public SaveContract AddComment(CommentContract comment)
        {
            return commentRepository.AddComment(comment);
        }

        public SaveContract Register(UserContract user)
        {
            return userRepository.Register(user);
        }

        public PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging)
        {
            return userRepository.GetUsers(filter, paging);
        }

        public PagedListResultContract<EntryContract> GetEntries(EntryFilterContract filter, PagedListRequestContract paging)
        {
            return entryRepository.GetEntries(filter, paging);
        }

        public PagedListResultContract<EntryContract> GetFavouritesEntriesForUser(EntryFilterContract filter, PagedListRequestContract paging)
        {
            return entryRepository.GetFavouritesImagesForUser(filter, paging);
        }

        public SaveContract ChangePassword(string login, string oldPassword, string newPassword)
        {
            return userRepository.ChangePassword(login, oldPassword, newPassword);
        }

        public bool HideImage(int imageId)
        {
            return entryRepository.HideImage(imageId);
        }

        public bool AddToMain(int imageId)
        {
            return entryRepository.AddToMain(imageId);
        }

        public bool BanUser(int userId, string banReason)
        {
            return userRepository.BanUser(userId, banReason);
        }

        public Dictionary<int, string> Users(string term)
        {
            return userRepository.Users(term);
        }

        public bool UnBanUser(int userId)
        {
            return userRepository.UnBanUser(userId);
        }

        public bool SetPermissions(int userId, bool isMod, bool IsAdmin)
        {
            return userRepository.SetPermissions(userId, isMod, IsAdmin);
        }

        public SaveContract AddEntry(EntryContract entry)
        {
            return entryRepository.AddEntry(entry);
        }

        public SaveContract AddImageToFavourites(EntryContract entry, int userId)
        {
            return entryRepository.AddImageToFavourites(entry, userId);
        }

        public SaveContract AddEntryCategories(int entryId, List<CategoryContract> categories)
        {
            return categoryRepository.AddCategories(entryId, categories);
        }

        public PagedListResultContract<CategoryContract> GetCategories(CategoryFilterContract filter, PagedListRequestContract paging)
        {
            return categoryRepository.GetCategories(filter, paging);
        }

        public SaveContract Create(CategoryContract category)
        {
            return categoryRepository.Create(category);
        }

        public int GetLoginUserId(string loginUser)
        {
            return userRepository.GetLoginUserId(loginUser);
        }

        public bool ImageAlreadyInFavourites(int entryId, string userName)
        {
            return entryRepository.ImageAlreadyInFavourites(entryId, userName);
        }

        public SaveContract VoteForEntry(EntryContract entry, int userId)
        {
            return entryRepository.VoteForEntry(entry, userId);
        }

        public bool EntryAlreadyVotedFor(int entryId, string userName)
        {
            return entryRepository.EntryAlreadyVotedFor(entryId, userName);
        }

        public int VotesNumber(int entryId)
        {
            return entryRepository.VotesNumber(entryId);
        }

        public int CommentVotesNumber(int userId, int commentId){

            return entryRepository.CommentVotesNumber(userId, commentId);
        }

        public bool CommentAlreadyVotedFor(int userId, int commentId)
        {
            return entryRepository.CommentAlreadyVotedFor(userId, commentId);
        }

        public SaveContract AddVoteForComment(int userId, int commentId)
        {
            return entryRepository.AddVoteForComment(userId, commentId);
        }
    }
}

