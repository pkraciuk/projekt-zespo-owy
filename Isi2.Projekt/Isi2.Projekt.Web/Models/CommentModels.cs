﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Isi2.Projekt.Web.Models
{
    public class CommentModels
    {
        [Required]
        [Display(Name = "Użytkownik")]
        public string UserName { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [Display(Name = "Komentarz")]
        public string Text { get; set; }
       
        [Required]
        [Display(Name = "Data dodania")]
        public string Date { get; set; }
    }
}