﻿using Isi2.Projekt.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isi2.Projekt.Web.Models.ViewModels
{
    public class EntryViewModel
    {
        public IEnumerable<EntryContract> Entries { get; set; }

        public UserContract CurentUser { get; set; }

    }
}
