﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Web.Code;
using Isi2.Projekt.Contracts.User;
using Isi2.Projekt.Web.Helpers;
using Isi2.Projekt.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Isi2.Projekt.Contracts.Common;
using Castle.Windsor;
using System.IO;
using Isi2.Projekt.Contracts.Entry;
using Isi2.Projekt.Contracts.Comment;

namespace Isi2.Projekt.Web.Controllers
{
    public class EntryController : Controller
    {
        private readonly IService service = WindsorContainerAccess.GetContainer().Resolve<IService>();

        public ActionResult Index(int page = 1)
        {
            var result = service.GetEntries(
                new EntryFilterContract
                    {
                        Published = true,
						Username=User.Identity.Name,
                        Deleted = false
                    },
                new PagedListRequestContract {Page = page - 1, PageSize = 10, Take = 10});
            return View(result);
        }

        public ActionResult Others(int page = 1)
        {
            var result = service.GetEntries(
                new EntryFilterContract
                    {
                        Published = false,
						Username=User.Identity.Name,
                        Deleted = false
                    },
                new PagedListRequestContract {Page = page - 1, PageSize = 10, Take = 10});
            return View(result);
        }

        [Authorize]
        [AccessGroup]
        [HttpGet]
        public void HideImage(int imageId)
        {
            service.HideImage(imageId);
            //return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult AddToMain(int imageId)
        {
            service.AddToMain(imageId);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult UploadImage()
        {
            return View();
        }

        [Authorize]
        public ActionResult GetFavouritesImagesForUser(int page = 1)
        {
            string userName = User.Identity.Name;
            int id = service.GetUsers(new UserFilterContract { Login = userName }, null).Items.First().Id;
            var result = service.GetFavouritesEntriesForUser(new EntryFilterContract { Id = id, Username = User.Identity.Name }, new PagedListRequestContract { Page = page - 1, PageSize = 10, Take = 10 });
            return View(result);
        }

        [Authorize]
        public ActionResult AddComment(int entryId, string text, int userId)
        {
            int idusera = service.GetUsers(new UserFilterContract { Id = userId }, null).Items.First().Id;
            var entry = service.GetEntries(new EntryFilterContract { Id = entryId }, null).Items.First().Id;
            var result = service.AddComment(new CommentContract()
            {
                EntryId = entry,
                UserId = idusera,
                Date = DateTime.Now,
                Text = text,
            });
            return View();
        }

        [HttpPost]
        public JsonResult Comments(int entryId)
        {
               var result = service.GetComments(
               new CommentFilterContract { EntryId = entryId },

               new PagedListRequestContract { Page = 0, PageSize = 10, Take = 10 });
                var jsonList = new List<object> { };
                if (result.Count > 0)
                {
                    result.Success = true;
                    foreach (var item in result.Items)
                     {
                        jsonList.Add(new
                        {
                        id = item.Id,
                        text = item.Text,
                        date = item.Date.ToShortDateString(),
                        user_name = item.UserName,
                        voted_for = service.CommentAlreadyVotedFor(service.GetUsers(new UserFilterContract { Login = item.UserName }, null).Items.First().Id,item.Id),
                        votes_number = service.CommentVotesNumber(service.GetUsers(new UserFilterContract { Login = item.UserName }, null).Items.First().Id,item.Id)  //user id service.GetUsers(new UserFilterContract { Login = userName }, null).Items.First().Id
                    });
                    }
                }
                //testowe wygenerowanie komentarzy //TODO: usunąć po testach
                //else 
                //{
                //    result.Success = true;
                //    for(int i=0;i<10;i++)
                //        jsonList.Add(new
                //        {
                //        id = 1,
                //        text = "testujeaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa<br/><br/><br/><br/>Symulacja długich komentarzy.",
                //        date = DateTime.Now.ToShortDateString(),
                //        user_name = "testowo" });
                    
                //}
               
            return Json(jsonList.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadImage(ImageUploadModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid && file != null)
            {
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/images"), fileName);
                    file.SaveAs(path);
                    //dodawanie obrazka
                    var result = service.AddEntry(new EntryContract()
                        {
                            Title = string.Empty,
                            AuthorId =
                                service.GetUsers(new Contracts.User.UserFilterContract {Login = model.AuthorId}, null).Items[0].Id,
                            Created = DateTime.Now,
                            Path = file.FileName
                        });
                    if (result.Success)
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Wystąpił błąd, spróbuj ponownie");
                }
            }
            return View();
        }

        [Authorize]
        public ActionResult AddImageToFavourite(int entryId,string userLogin)
        {
            if (service.ImageAlreadyInFavourites(entryId, userLogin)){
                return View();
            }
            int id = service.GetUsers(new UserFilterContract{Login = userLogin}, null).Items.First().Id;
            var entry = service.GetEntries(new EntryFilterContract { Id = entryId }, null).Items.First();
            service.AddImageToFavourites(entry, id);
            return View();          
        }

        [Authorize]
        public ActionResult VoteForEntry(int entryId, string userLogin)
        {         
            if (!service.EntryAlreadyVotedFor(entryId, userLogin)){
                return View();
            }
            int id = service.GetUsers(new UserFilterContract { Login = userLogin }, null).Items.First().Id;
            var entry = service.GetEntries(new EntryFilterContract { Id = entryId }, null).Items.First();
            service.VoteForEntry(entry, id);
            return View();
        }

        [Authorize]
        public ActionResult VoteForComment(string userName, int commentId)
        {
            int userId = service.GetUsers(new UserFilterContract { Login = userName }, null).Items.First().Id;
            //throw new DivideByZeroException();
            if (service.CommentAlreadyVotedFor(userId,commentId)){
                return View();
            }
            service.AddVoteForComment(userId, commentId);
            return View();
        }
    }
}
