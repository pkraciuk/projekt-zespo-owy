﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts
{
    [DataContract]
    public class EntryContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public int AuthorId { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public int CategoryId { get; set; }

        [DataMember]
        public bool Published { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string File { get; set; }

        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public bool Sticky { get; set; }
        
		[DataMember]
        public int VotesNumber { get; set; }

        [DataMember]
        public bool ImageAlreadyInFavourites { get; set; }

        [DataMember]
        public bool EntryAlreadyVotedFor { get; set; }

    }
}
