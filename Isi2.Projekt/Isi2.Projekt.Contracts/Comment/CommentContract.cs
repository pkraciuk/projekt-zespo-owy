﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts.Comment
{
    [DataContract]
    public class CommentContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int EntryId { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool CommentAlreadyVotedFor { get; set; }

        [DataMember]
        public int CommentVotesNumber { get; set; }
    }
}
