﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts
{
    [DataContract]
    public class UserContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Email { get; set; }
       
        [DataMember]
        public string City { get; set; }

        [DataMember]
        public bool? Banned { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Password_reminder { get; set; }

        [DataMember]
        public DateTime? Date_last_login { get; set; }

        [DataMember]
        public DateTime Date_created { get; set; }

        [DataMember]
        public string Ban_reason { get; set; }

        [DataMember]
        public bool? Is_mod { get; set; }

        [DataMember]
        public bool? Is_admin { get; set; } 
    }
}