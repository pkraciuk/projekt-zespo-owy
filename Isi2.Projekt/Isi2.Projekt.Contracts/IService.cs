﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Security;
using Isi2.Projekt.Contracts.Entry;
using Isi2.Projekt.Contracts.Comment;


namespace Isi2.Projekt.Contracts
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging);

        [OperationContract]
        SaveContract Register(UserContract user);

        [OperationContract]
        SaveContract ChangePassword(string login, string oldPassword, string newPassword);

        [OperationContract]
        PagedListResultContract<EntryContract> GetEntries(EntryFilterContract filter, PagedListRequestContract paging);

        [OperationContract]
        bool HideImage(int imageId);

        [OperationContract]
        bool AddToMain(int imageId);

        [OperationContract]
        bool BanUser(int userId, string banReason);

        [OperationContract]
        Dictionary<int, string> Users(string term);

        [OperationContract]
        bool UnBanUser(int userId);

        [OperationContract]
        bool SetPermissions(int userId, bool isMod, bool IsAdmin);

        [OperationContract]
        SaveContract AddEntry(EntryContract entry);

        [OperationContract]
        SaveContract AddEntryCategories(int entryId, List<CategoryContract> categories);

        [OperationContract]
        PagedListResultContract<CategoryContract> GetCategories(CategoryFilterContract filter, PagedListRequestContract paging);

        [OperationContract]
        SaveContract Create(CategoryContract category);
         
        [OperationContract]
        PagedListResultContract<EntryContract> GetFavouritesEntriesForUser(EntryFilterContract entryFilterContract, PagedListRequestContract pagedListRequestContract);


        [OperationContract]
        SaveContract AddImageToFavourites(EntryContract entry, int userId);

        [OperationContract]
        PagedListResultContract<CommentContract> GetComments(CommentFilterContract filter, PagedListRequestContract paging);

        [OperationContract]
        SaveContract AddComment(CommentContract comment);

		[OperationContract]
        int GetLoginUserId(string loginUser);

        [OperationContract]
        bool ImageAlreadyInFavourites(int entryId, string userName);

        [OperationContract]
        SaveContract VoteForEntry(EntryContract entry, int userId);

        [OperationContract]
        bool EntryAlreadyVotedFor(int entryId, string userName);

        [OperationContract]
        int VotesNumber(int entryId);

        [OperationContract]
        int CommentVotesNumber(int userId, int commentId);

        [OperationContract]
        bool CommentAlreadyVotedFor(int userId, int commentId);

        [OperationContract]
        SaveContract AddVoteForComment(int userId, int commentId);
    }
}
